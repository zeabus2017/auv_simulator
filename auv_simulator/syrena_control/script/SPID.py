#!/usr/bin/env python

import rospy
from time import time


class SPID(object):
    def __init__(self, KP=0, KI=0, KD=0, ttl=100):
        self.KP, self.KI, self.KD = KP, KI, KD

        self.est = 0.001
        self._ttl = ttl
        self.ttl = ttl

        self.P, self.I, self.D = 0, 0, 0
        self.pevErr = 0

        self.time = time()

    def reset(self):
        # self.pevErr=0
        self.P, self.I, self.D = 0, 0, 0
        self.ttl = self._ttl

    def sat(self, a, bmax, bmin):
        if a > bmax:
            return bmax
        if a < bmin:
            return bmin
        return a

    def get(self):
        return self.KP, self.KI, self.KD

    def set(self, KP, KI, KD):
        self.KP, self.KI, self.KD = KP, KI, KD
        print "SET :", self.KP, self.KI, self.KD

    def pid(self, err):  # err = setpoint-actual_position

        dt = self.get_deltatime()

        if self.ttl == 0:
            # print 'reset pid'
            self.reset()
        self.P = err
        if abs(err) > self.est:
            self.I += err * dt
        self.D = (err - self.pevErr) / dt
        out = (self.P * self.KP) + (self.I * self.KI) + (self.D * self.KD)
        out = self.sat(out, 2, -2)
        self.pevErr = err
        self.ttl -= 1
        return out

    def get_deltatime(self):
        temp, self.time = self.time, time()

        return self.time - temp
