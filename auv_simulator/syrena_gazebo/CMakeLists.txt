cmake_minimum_required(VERSION 2.8.3)
project(syrena_gazebo)

find_package(catkin REQUIRED COMPONENTS
    roscpp
    sensor_msgs
    nav_msgs
    robot_localization
    move_base
)

catkin_package(
 LIBRARIES ${PROJECT_NAME}
 DEPENDS
    roscpp
    sensor_msgs
    nav_msgs
    robot_localization
    move_base
)

include_directories(${catkin_INCLUDE_DIRS})

# demo node