#!/usr/bin/env python

import rospy

import numpy as np

from geometry_msgs.msg import Twist,PoseWithCovarianceStamped
from sensor_msgs.msg import LaserScan
from laser_geometry import LaserProjection
import sensor_msgs.point_cloud2 as pc2

import math

from math import pi

import scipy.stats as st
from plot import Plot

from least_sqr_circle import leastsq_circle, plot_data_circle

#from rolling_window import rolling_window

from itertools import imap

from time import time


def rolling_window_index(seq, size=2, step=1):
    for i in xrange(int(seq / step + 1)):

        l = i * step
        r = l + size

        if r >= seq:
            yield seq - size, seq
            return

        yield (l, l + size)


def running_mean(x, n):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[n:] - cumsum[:-n]) / n


def split_non_adjacent(rows):

    l = []

    prev = -2
    for r in rows:

        if prev + 1 != r:

            l.append([r])

        else:

            l[-1].append(r)

        prev = r

    return np.array(l)


class PipeTracker(object):
    def __init__(self):
        rospy.init_node("pipe_tracker")

        self.first = True

        self.scan = None
        self.cmd = None

        self.laser_projector = LaserProjection()

        self.pub_cmd = rospy.Publisher("/cmd_vel", Twist, queue_size=1)

        self.pub_pipe_pose = rospy.Publisher("/syrena/ai/pipe_pose", PoseWithCovarianceStamped, queue_size=1)
        # self.pub_pipe_pose = rospy.Publisher("/syrena/ai/pipe_test", PoseWithCovarianceStamped, queue_size=1)

        rospy.Subscriber("/scan", LaserScan, self.laser_update)

    def laser_preprocess(self, laser):

        cloud = self.laser_projector.projectLaser(laser)
        gen = pc2.read_points(cloud, skip_nans=True, field_names=("x", "y"))

        scan = np.array(list(gen))

        y, xp = scan.T

        x = np.linspace(xp[0], xp[-1], xp.size)
        y = np.interp(x, xp, y)

        y = - y[::-1]

        return np.array([x, y]).T

    def laser_update(self, data):

        scan = self.laser_preprocess(data)
        if self.first:
            self.first = False

        else:
            self.scan = scan

    def run(self):

        rate = rospy.Rate(100)

        while self.first and rospy.is_shutdown():
            pass

        rospy.sleep(0.5)

        t = time()

        self.not_found = 0

        lim_x, lim_y = (-3, 3), (-6, 0)

        disable = False

        # raw_graph = Plot(text="raw+pipe", lim_x=lim_x,
        #                  lim_y=lim_y, disable=disable)

        targeted_no = 9
        circle_graph = Plot(dimension=33, n=targeted_no, text="targeted circles",
                            lim_x=lim_x, lim_y=lim_y, disable=1)

        self.pipe_R = 0.20 # m

        while not rospy.is_shutdown():

            scan = self.scan  # [270:400]

            axis, data = scan.T

            axis_per_m = 1.0 * axis.size / (axis[-1] - axis[0])

            # n = 30
            # data = running_mean(data, n=n)
            # axis = axis[n - 1:]
            # scan = np.array([axis, data]).T

            window_size = self.pipe_R*axis_per_m
            print "window_size :",window_size
            windows = rolling_window_index(seq=axis.size, size=window_size*1.5, step=window_size/8)





            circles = list(self.find_circle_fit(windows, scan))

            print "circles :",len(circles)


            #i = 0
            pipes = []

            circles = [circle for circle in circles if abs(circle[2]-self.pipe_R)/self.pipe_R < 0.3 ]

            if not circles: continue

            circles = sorted(circles, key=lambda x: x[3])[:8]#abs(x[2]-self.pipe_R))

            print "pipe_R = %.3f"%(self.pipe_R)

            alpha = 0.85
            self.pipe_R = (alpha * self.pipe_R) + ((1-alpha) * circles[0][2])#* sum( circle[2] for circle in circles)/ len(circles) )

            for xc, yc, R, residu in circles:


                text = "R = %.4f ; residu = %.4f" % (R, residu)

                theta_fit = np.linspace(-pi, pi, 25)

                x_fit = xc + R * np.cos(theta_fit)
                y_fit = yc + R * np.sin(theta_fit)

                pipes.append((xc, yc, R, residu))

                print "R= %.3f : residu=%.5f" % (R, residu)

                axis = np.append(axis, x_fit)
                data = np.append(data, y_fit)


            # raw_graph.update(axis, data, lim_x=lim_x, lim_y=lim_y)



            self.move(pipes)
            self.report_pipe_pose(pipes)

            T = time()
            print "time per loop = %.3f s" % (T - t)
            t = T

            rate.sleep()

    def report_pipe_pose(self, pipes):

        pipe_pose = PoseWithCovarianceStamped()

        pipe_pose.header.stamp = rospy.Time.now()
        pipe_pose.header.frame_id = "base_link"


        pipe_pose.pose.covariance = [ 0.1**2 if i%6 ==0 else 0 for i in xrange(36)]

        if pipes:
            y, z, R, residu = pipes[0]

            pipe_pose.pose.pose.position.y = y

            print pipe_pose
            # print y,-z

            self.pub_pipe_pose.publish(pipe_pose)


    def move(self, pipes):

        t = Twist()

        t.linear.x = 0.2
        t.linear.z = 0

        if pipes:
            xc, yc, R, residu = pipes[0]

            t.linear.x = 0.8 - min(0.8,residu *1000)
            t.linear.y = -xc# * 0.2
            t.linear.z = (yc + R + 1.8) * 0.4

            self.not_found = 0
        else:

            self.not_found += 1

            if self.not_found > 5:

                t.linear.x = 0

        t.linear.x = 0
        t.linear.z = 0

        self.pub_cmd.publish(t)

    def find_circle_fit(self, windows, scan, index=False):

        for l,r in windows:

            period_point = scan[l:r]

            x_pp, y_pp = period_point.T

            xc, yc, R, residu = leastsq_circle(x_pp, y_pp)

            if R>1:
                continue
            if yc > np.interp(xc, x_pp, y_pp):
                continue

            yield xc, yc, R, residu

if __name__ == '__main__':

    p = PipeTracker()
    p.run()
