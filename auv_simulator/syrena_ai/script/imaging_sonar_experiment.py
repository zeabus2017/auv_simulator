#!/usr/bin/env python


import rospy

import numpy as np

# from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan, PointCloud2
import sensor_msgs.point_cloud2 as pc2


from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import math

from math import pi

from plot import Plot
import cv2

import os
#from tf2_sensor_msgs.tf2_sensor_msgs import do_transform_cloud


class KinectToSonar(object):
    def __init__(self, arg=dict()):
        self.arg = arg

        try:
            rospy.init_node("kinect_to_sonar")
        except:
            pass

        rospy.Subscriber("/imaging_sonar/depth/points",
                         PointCloud2, self.cloud_update, queue_size=1)

        self.x = None
        self.y = None
        self.z = None

        self.r = None
        self.theta = None

    def cloud_update(self, cloud):
        self.points = pc2.read_points(
            cloud, skip_nans=True, field_names=("r", "theta", "ra"))

    def rotate_image_to_polar(self, img):

        x, y = np.mgrid[:self.sonar_r_pix, :self.sonar_view_size]

        x = self.sonar_r_pix - x - 1
        y = y - self.sonar_view_size / 2

        r = np.sqrt((x)**2 + (y)**2)
        theta = (np.arctan2(y, x) * 180. / np.pi)
        thetapic = (theta + (90 - self.sonar_angle_shift)) * 3

        return cv2.remap(img[::-1, ::], thetapic.astype('float32'), r.astype('float32'), cv2.INTER_LANCZOS4)

    def run(self):

        msg = rospy.wait_for_message(
            "/imaging_sonar/depth/points", PointCloud2)

        rate = rospy.Rate(5)

        self.sonar_range = 50
        self.sonar_r_scale = 10
        self.sonar_r_pix = self.sonar_r_scale * self.sonar_range

        self.sonar_angle_scale = 3  # pix per deg
        self.sonar_fov = 130
        self.sonar_theta_pix = self.sonar_fov * self.sonar_angle_scale

        self.sonar_angle_shift = 90 - self.sonar_fov / 2

        self.sonar_view_size = round(math.cos(math.radians(
            self.sonar_angle_shift)) * 2 * self.sonar_r_pix) + 1

        while not rospy.is_shutdown():

            os.system('cls' if os.name == 'nt' else 'clear')

            r, theta, ra = np.array(list(self.points))[::3].T

            r = np.rint(r * self.sonar_r_scale)

            theta = (theta * 180 / np.pi) +self.sonar_fov/2
            # print(theta.min(),theta.max())
            theta = np.rint(theta * self.sonar_angle_scale)

            gen = np.zeros((self.sonar_r_pix, self.sonar_theta_pix + 1))

            print "point size \n  :", r.size

            print(gen.shape)
            for i in xrange(r.size):
                    # c = color[i,j,2]/25
                    # c = 0 if c < 5 else c
                    # print c,
                if r[i] >= self.sonar_r_pix:
                    continue

                if ra[i] <= 0.1736: # theshold for critical angle
                    continue

                I = ((self.sonar_r_pix-r[i])/self.sonar_r_pix)**1.2
                # I*= ra[i]*

                gen[r[i], theta[i]] += I

            print "raw \n  : %5.3f %5.3f" % (gen.min(), gen.max())

            gen = gen[::-1, ::]

            cv2.imshow("raw", gen)
            cv2.moveWindow("raw", 75, 10)

            gen = cv2.blur(gen, (3, 3))
            gen = cv2.GaussianBlur(gen, (3, 9), 10, 10)

            print "before \n  : %5.3f %5.3f" % (gen.min(), gen.max())

            cv2.imshow("before", gen)
            cv2.moveWindow("before", 75, 500)

            noise = np.zeros((self.sonar_r_pix, self.sonar_theta_pix + 1))
            noise = cv2.randn(noise, 0, 1.5)

            gen = np.exp(np.log(gen + 0.01) + noise)

            print "after \n  : %5.3f %5.3f" % (gen.min(), gen.max())
            print

            cv2.imshow("after", gen)
            cv2.moveWindow("after", self.sonar_theta_pix + 75 + 10, 500)

            rotated = self.rotate_image_to_polar(gen)

            cv2.imshow("rotated", rotated)
            cv2.moveWindow("rotated", self.sonar_theta_pix + 75 + 10, 10)

            cv2.waitKey(1)
            rate.sleep()

        rospy.spin()


if __name__ == '__main__':

    x = KinectToSonar()
    x.run()
