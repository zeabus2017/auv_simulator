import cv2
import numpy as np

import matplotlib.pyplot as plt

from sonar_raw import *

def hist_match(source, template):
    """
    Adjust the pixel values of a grayscale image such that its histogram
    matches that of a target image

    Arguments:
    -----------
        source: np.ndarray
            Image to transform; the histogram is computed over the flattened
            array
        template: np.ndarray
            Template image; can have different dimensions to source
    Returns:
    -----------
        matched: np.ndarray
            The transformed output image
    """

    oldshape = source.shape
    source = source.ravel()
    # template = template.ravel()

    # get the set of unique pixel values and their corresponding indices and
    # counts
    s_values, bin_idx, s_counts = np.unique(source, return_inverse=True,
                                            return_counts=True)
    t_values, t_counts = np.unique(template, return_counts=True)

    # take the cumsum of the counts and normalize by the number of pixels to
    # get the empirical cumulative distribution functions for the source and
    # template images (maps pixel value --> quantile)
    s_quantiles = np.cumsum(s_counts).astype(np.float64)
    s_quantiles /= s_quantiles[-1]
    t_quantiles = np.cumsum(t_counts).astype(np.float64)
    t_quantiles /= t_quantiles[-1]

    # interpolate linearly to find the pixel values in the template image
    # that correspond most closely to the quantiles in the source image
    interp_t_values = np.interp(s_quantiles, t_quantiles, t_values)

    return interp_t_values[bin_idx].reshape(oldshape)

if __name__ == '__main__':

    path = "/home/universez/Desktop/sonar_col/exported/slam_middle_kuan/"

    
    template = np.loadtxt('hist_avi.out')#,fmt='%d')

    # print template.shape

    i = 0

    x,y = 0,0
    data_x = []
    data_y = []

    limit = None#100

    start = 270

    last_polar = None

    for ping in get_sonar_data(path):
        # print ping['image']

        # print ping
        i += 1

        if limit and start and i< start: continue

        if limit and i >= limit+start:
            break
        
        img = ping['image']

        # x,y = 0,100; cv2.rectangle(img, (x, y), (x+20, y+20), (255,0,0), 3);
        # x,y = ping['width']-100,100; cv2.rectangle(img, (x, y), (x+20, y+20), (150,0,0), 6);
        # x,y = ping['width']-100,ping['height']-100; cv2.rectangle(img, (x, y), (x+20, y+20), (255,0,0), 9)
        # x,y = 0,ping['height']-100; cv2.rectangle(img, (x, y), (x+20, y+20), (255,0,0), 12)
        # print ping

        # polar = cart_to_polar(ping)
        # cv2.imshow('cart', polar)
        # polar = cart_to_polar(ping)

        
        theta_res,polar = cart_to_polar(ping)
        # theta_res,polar = cart_to_polar_remap(ping)

        # polar = polar#/64.0*256
        
        idx = polar[:,:] < 0.01 * 256 
        polar[idx] = 0

        polar = polar/256.0

        # idx = polar[:,:] > 0.03 
        # polar[idx] *=2

        # idx = polar[:,:] > 1 
        # polar[idx] *=1


        # polar = hist_match(polar,template)/256.0
        print polar.max()#cv2.minMaxLoc(polar)
        polar = np.float32(polar)


        if True:
            # cv2.imshow('raw', img)
            cv2.imshow('polar', polar)

            # cv2.waitKey(100)
        
        polar = cv2.GaussianBlur(polar,(27,27),1)
        # polar = np.log(polar+1)

        
        # polar = np.

        # print cv2.minMaxLoc(polar)

        if last_polar is not None:
            d = cv2.phaseCorrelate(last_polar, polar)

            # if d[-1] < 0.8: print d[-1]
            dx, dy = d[0]

            if abs(dy) >= 100:
                print "><"*50,dy
                dy = 0

            # print i,dx, dy, d[-1]
            print "%d %7.4f %7.4f %7.4f"%(i,dx,dy,d[-1])#*0.3217821782178218
            x += dx
            y += dy# * theta_res  # *FRAME_RATE

            data_x += [x]
            data_y += [y]

        last_polar =  np.float32(polar)


        # print
        # cv2.setMouseCallback("polar",on_mouse,param=None)


        if 0xFF & cv2.waitKey(1) == 27:
            break
        # cv2.waitKey()
        # exit()

    # d1-d2.total_seconds()
    cv2.destroyAllWindows()

    plt.scatter(data_y, range(len(data_y)))

    plt.grid(True)

    plt.axes().set_aspect('equal', 'datalim')

    plt.show()

    # main()
